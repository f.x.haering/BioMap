using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BioMap.Shared;
using ChartJs.Blazor;
using ChartJs.Blazor.BarChart;
using ChartJs.Blazor.BarChart.Axes;
using ChartJs.Blazor.Common;
using ChartJs.Blazor.Common.Axes;
using ChartJs.Blazor.Common.Axes.Ticks;
using ChartJs.Blazor.Common.Enums;
using ChartJs.Blazor.Common.Handlers;
using ChartJs.Blazor.Common.Time;
using ChartJs.Blazor.LineChart;
using ChartJs.Blazor.Util;
using GoogleMapsComponents;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BioMap.Pages.Statistics
{
  public partial class SurvivalRate : ProtectedPage
  {
    [Inject]
    protected DataService DS { get; set; }
    [Inject]
    protected SessionData SD { get; set; }
    //
    private int ProjectYearBegin;
    private int ProjectYearEnd;
    private BarConfig _configOverTime;
    private Chart _chartJsOverTime;
    private TableFromChart _tableFromChartOverTime;
    //
    protected override void OnInitialized() {
      base.OnInitialized();
      this.ProjectYearBegin = this.SD.CurrentProject.StartDate.Value.Year;
      this.ProjectYearEnd = Math.Max(this.ProjectYearBegin + 1, (DateTime.Now - TimeSpan.FromDays(90)).Year);
      this._configOverTime = new BarConfig {
        Options = new BarOptions {
          Animation = new Animation {
            Duration = 0,
          },
          Title = new OptionsTitle {
            Text = "XXX",
            Display = false,
          },
          Legend = new Legend {
            Display = true,
          },
          Scales = new BarScales {
            XAxes = new List<CartesianAxis> {
                new BarCategoryAxis {
                    Stacked = false,
                },
            },
            YAxes = new List<CartesianAxis> {
              new BarLinearCartesianAxis {
                ID="cnt",
                ScaleLabel=new ScaleLabel {
                  LabelString=this.Localize["Count"],
                  Display=true,
                },
                Stacked = false,
                Ticks=new LinearCartesianTicks {
                  Min=0,
                },
              },
            },
          },
        },
      };
      this.SD.Filters.FilterChanged += this.Filters_FilterChanged;
      this.RefreshData();
    }
    protected override async Task OnAfterLeavePage() {
      this.SD.Filters.FilterChanged -= this.Filters_FilterChanged;
    }
    private void Filters_FilterChanged(object sender, EventArgs e) {
      this.RefreshData();
      this.StateHasChanged();
    }
    protected override async Task OnAfterRenderAsync(bool firstRender) {
      await base.OnAfterRenderAsync(firstRender);
      if (firstRender) {
      }
      this._tableFromChartOverTime.RefreshData();
    }
    private void RefreshData() {
      this._configOverTime.Data.Labels.Clear();
      this._configOverTime.Data.Datasets.Clear();
      //
      Dictionary<int, List<Element>> aaIndisByIId = this.DS.GetIndividuals(this.SD, this.SD.Filters);
      if (aaIndisByIId.Keys.Count() >= 1) {
        var dtMin = aaIndisByIId.Values.Min(els => els.Min(el => el.ElementProp.CreationTime));
        int yearBegin = dtMin.Year;
        aaIndisByIId = this.DS.GetIndividuals(this.SD, this.SD.Filters, null, true);
        string sIIds = string.Join(' ', aaIndisByIId.Keys.Select(iid => ConvInvar.ToString(iid)).ToArray());
        Dictionary<int, string> dictIId2Gender = new();
        foreach (int nIId in aaIndisByIId.Keys) {
          dictIId2Gender.Add(nIId, aaIndisByIId[nIId][^1].Gender);
        }
        for (int year = yearBegin; year <= this.ProjectYearEnd; year++) {
          this._configOverTime.Data.Labels.Add(ConvInvar.ToString(year));
        }
        {
          var dsMales = new BarDataset<int>() {
            YAxisId = "cnt",
            Label = this.Localize["Male"],
            BackgroundColor = ChartJs.Blazor.Util.ColorUtil.FromDrawingColor(System.Drawing.Color.FromArgb(200, System.Drawing.Color.Blue)),
          };
          var dsFemales = new BarDataset<int>() {
            YAxisId = "cnt",
            Label = this.Localize["Female"],
            BackgroundColor = ChartJs.Blazor.Util.ColorUtil.FromDrawingColor(System.Drawing.Color.FromArgb(200, System.Drawing.Color.Pink)),
          };
          var dsAll = new BarDataset<int>() {
            YAxisId = "cnt",
            Label = this.Localize["All"],
            BackgroundColor = ChartJs.Blazor.Util.ColorUtil.FromDrawingColor(System.Drawing.Color.FromArgb(200, System.Drawing.Color.Gray)),
          };
          var dictIIdsByYear = new Dictionary<int, int[]>();
          for (int year = yearBegin; year <= this.ProjectYearEnd; year++) {
            var filters = new Filters(() => this.SD.CurrentUser);
            filters.FilteringTarget = Filters.FilteringTargetEnum.Catches;
            filters.DateFromFilter = new DateTime(year, 1, 1);
            filters.DateToFilter = new DateTime(year, 12, 31);
            filters.IndiFilter = sIIds;
            Dictionary<int, List<Element>> aaIndisByIIdInYear = this.DS.GetIndividuals(this.SD, filters);
            dictIIdsByYear.Add(year, aaIndisByIIdInYear.Keys.ToArray());
          }
          for (int year = this.ProjectYearEnd - 1; year > yearBegin; year--) {
            dictIIdsByYear[year] = dictIIdsByYear[year].Union(dictIIdsByYear[year + 1]).ToArray();
          }
          for (int year = yearBegin; year <= this.ProjectYearEnd; year++) {
            int[] aIIdsInYear = dictIIdsByYear[year];
            int nMales = 0;
            int nFemales = 0;
            int nAll = aIIdsInYear.Count();
            foreach (int nIId in aIIdsInYear) {
              if (dictIId2Gender.TryGetValue(nIId, out string sGender)) {
                if (sGender == "m") {
                  nMales++;
                } else if (sGender == "f") {
                  nFemales++;
                }
              }
            }
            dsMales.Add(nMales);
            dsFemales.Add(nFemales);
            dsAll.Add(nAll);
          }
          this._configOverTime.Data.Datasets.Add(dsMales);
          this._configOverTime.Data.Datasets.Add(dsFemales);
          this._configOverTime.Data.Datasets.Add(dsAll);
        }
      }
    }
  }
}
