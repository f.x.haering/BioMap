
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace BioMap.Shared
{
  /// <summary>
  /// This class fires an event a specified delay after it had been triggered 
  /// for the last time.
  /// </summary>
  [ToolboxItem(true)]
  [DefaultEvent("Elapsed")]
  public sealed class Watchdog
  {

    private int msBackTimer = -1;         // -1=wait.
    private int msCurrentTime;

    public Watchdog(int nDelayMs)
      : base() {
      this.Delay = nDelayMs;
    }

    /// <summary>
    /// An identifier for debug purposes.
    /// </summary>
    public int DebugId = 0;

    /// <summary>
    /// (Re-)trigger of the delay mechanism. A call to this method delays
    /// execution of the callback by the time specified in the constructor.
    /// </summary>
    public void Trigger() {
      if (this.Enabled) {
        lock (staticLockObject) {
          this.msBackTimer = this.Delay;
          this.msCurrentTime = System.Environment.TickCount;
          if (!waitingInstances.Contains(this)) {
            waitingInstances.Add(this);
          }
          if (staticThread == null) {
            staticThread = new Thread(new ThreadStart(RunThread)) {
              Name = "Watchdog"
            };
            staticThread.Start();
          }
        }
      }
    }

    /// <summary>
    /// true if triggered but not yet elapsed.
    /// </summary>
    public bool Triggered => this.msBackTimer >= 0;

    /// <summary>
    /// Force the <see cref="Elapsed"/> event immediately (but not synchroneously).
    /// </summary>
    public void ForceElapsed() {
      if (this.Enabled) {
        lock (staticLockObject) {
          if (!forcedInstances.Contains(this)) {
            forcedInstances.Add(this);
          }
          waitingInstances.Remove(this);
          if (staticThread == null) {
            staticThread = new Thread(new ThreadStart(RunThread)) {
              Name = "Watchdog"
            };
            staticThread.Start();
          }
        }
      }
    }

    /// <summary>
    /// Stop delay mechanism. No callback will be executed unless the 
    /// <see cref="Trigger"/> method is called again.
    /// </summary>
    public void Stop() {
      lock (staticLockObject) {
        waitingInstances.Remove(this);
        this.msBackTimer = -1;
      }
    }

    /// <summary>
    /// Control operation of the delay mechanism. If false, no <see cref="Elapsed"/>
    /// event will be fired and no call to the <see cref="Trigger"/> method will
    /// have any effect.
    /// </summary>
    [DefaultValue(true)]
    public bool Enabled {
      get => this._Enabled;
      set {
        if (value != this._Enabled) {
          this._Enabled = value;
          if (!value) {
            this.Stop();
          }
        }
      }
    }
    private bool _Enabled = true;

    /// <summary>
    /// The time in milliseconds after which the callback is executed when the Trigger() method was not called.
    /// </summary>
    [DefaultValue(500)]
    public int Delay { get; set; } = 500;

    /// <summary>
    /// The time in milliseconds until the <see cref="Elapsed"/> event will be fired if no
    /// call to <see cref="Trigger"/> or <see cref="Stop"/> occurs in the meantime.
    /// </summary>
    /// <returns>
    /// The time in milliseconds or -1 if no <see cref="Elapsed"/> event is expected.
    /// </returns>
    public int GetTimeToElapse() {
      return this.msBackTimer;
    }

    /// <summary>
    /// Fired when the <see cref="Delay"/> time has elapsed since the 
    /// most recent call to <see cref="Trigger"/>.
    /// </summary>
    public event EventHandler Elapsed;

    private void OnElapsed(EventArgs e) {
      this.Elapsed(this, e);
    }

    private static readonly List<Watchdog> waitingInstances = new List<Watchdog>();
    private static readonly List<Watchdog> forcedInstances = new List<Watchdog>();

    private static readonly object staticLockObject = new object();
    private static Thread? staticThread = null;

    /// <summary>
    /// Prepare for application shutdown; all instances will be quiet and no thread 
    /// will be active after calling this method.
    /// </summary>
    public static void ShutDown() {
      bShutDown = true;
    }
    private static bool bShutDown = false;

    private static void RunThread() {
      var callbacksToFire = new List<Watchdog>();
      int nBackTimer;
      bool bExit = bShutDown;
      while (!bExit) {
        callbacksToFire.Clear();
        lock (staticLockObject) {
          int msCurrentTime = System.Environment.TickCount;
          foreach (Watchdog watchdog in waitingInstances) {
            int msDiff = msCurrentTime - watchdog.msCurrentTime;
            watchdog.msCurrentTime = msCurrentTime;
            nBackTimer = watchdog.msBackTimer - msDiff;
            if (nBackTimer < 0) {
              watchdog.msBackTimer = -1;
              callbacksToFire.Add(watchdog);
            } else {
              watchdog.msBackTimer = nBackTimer;
            }
          }
          foreach (Watchdog watchdog in callbacksToFire) {
            waitingInstances.Remove(watchdog);
          }
          if (waitingInstances.Count < 1) {
            staticThread = null;
            bExit = true;
          }
          // Add forced instances.
          foreach (Watchdog watchdog in forcedInstances) {
            if (!callbacksToFire.Contains(watchdog)) {
              callbacksToFire.Add(watchdog);
            }
          }
          forcedInstances.Clear();
        }
        foreach (Watchdog watchdog in callbacksToFire) {
          watchdog.OnElapsed(EventArgs.Empty);
        }
        int nWaitTime = 20;
        try {
          Thread.Sleep(nWaitTime);
        } catch { }
      }
    }

  }
}

