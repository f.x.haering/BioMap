
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;

namespace BioMap.Shared
{
  public class StatisticalValue
  {

    public StatisticalValue() {
      this._Count = 0;
      this._Min = 0.0;
      this._Max = 0.0;
      this._Sum = 0.0;
      this._SquareSum = 0.0;
    }

    public StatisticalValue(IEnumerable<double> values) : this() {
      foreach (double value in values) {
        this.AddValue(value);
      }
    }

    public void AddValue(double value) {
      this._Sum += value;
      this._SquareSum += value * value;
      if (this._Count == 0 || value < this._Min) {
        this._Min = value;
      }
      if (this._Count == 0 || value > this._Max) {
        this._Max = value;
      }
      this._Count++;
    }

    public int Count => this._Count;
    private int _Count = 0;

    public double Sum => this._Sum;
    private double _Sum = 0.0;

    public double Max => this._Max;
    private double _Max = 0.0;

    public double Min => this._Min;
    private double _Min = 0.0;

    public double SquareSum => this._SquareSum;
    private double _SquareSum = 0.0;

    public double MeanValue {
      get {
        if (this._Count < 1) {
          return 0.0;
        } else {
          return this._Sum / this._Count;
        }
      }
    }

    public double Variance {
      get {
        if (this._Count < 2) {
          return 0.0;
        } else {
          double mv = this.MeanValue;
          return (this._SquareSum / this._Count) - (mv * mv);
        }
      }
    }

    public double StdDeviation {
      get {
        if (this._Count < 2) {
          return 0.0;
        } else {
          return Math.Sqrt(this.Variance);
        }
      }
    }

    public double StdError {
      get {
        if (this._Count < 2) {
          return 0.0;
        } else {
          return Math.Sqrt(this.Variance) / Math.Sqrt(this._Count);
        }
      }
    }

  }
}

