using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Routing;

namespace BioMap.Shared
{
  public partial class ProtectedPage : ComponentBase, IDisposable
  {
    [Inject]
    protected NavigationManager NM { get; set; } = default!;

    [Inject]
    protected SessionData SD { get; set; } = default!;

    [Parameter]
    public Func<SessionData, bool> IsTrusted { get; set; } = (sd) => sd.CurrentUser.Level >= 0;

    /// <summary>
    /// Interval in milliseconds for cyclic calls to CyclicTimer().
    /// </summary>
    [Parameter]
    public int TimerIntervalMs { get; set; } = 0;

    protected virtual async Task CyclicTimer() {
    }

    private System.Threading.Timer timer;
    private int CyclicTimerInProgress = 0;

    public void Dispose() {
      this.timer?.Dispose();
    }

    protected override async Task OnInitializedAsync() {
      await base.OnInitializedAsync();
      this.NM.LocationChanged += this.NM_LocationChanged;
      if (this.TimerIntervalMs > 0) {
        this.timer = new System.Threading.Timer(async _ => {
          try {
            if (Interlocked.CompareExchange(ref this.CyclicTimerInProgress, 1, 0) == 0) {
              try {
                await this.InvokeAsync(this.CyclicTimer);
                await this.InvokeAsync(this.StateHasChanged);
              } finally {
                Interlocked.Exchange(ref this.CyclicTimerInProgress, 0);
              }
            }
          } catch { }
        }, null, 0, this.TimerIntervalMs);
      }
    }
    protected override async Task OnAfterRenderAsync(bool firstRender) {
      if (firstRender) {
        if (!this.IsTrusted(this.SD)) {
          this.NM.NavigateTo("/");
        }
      }
      await base.OnAfterRenderAsync(firstRender);
    }
    protected virtual async Task OnAfterLeavePage() {
    }
    private void NM_LocationChanged(object? sender, LocationChangedEventArgs e) {
      this.NM.LocationChanged -= this.NM_LocationChanged;
      this.OnAfterLeavePage();
    }
    public Task InvokePubAsync(Action workItem) {
      return base.InvokeAsync(workItem);
    }
  }
}
